package com.xeranas.lkj.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MessageQueue {
	
	private static List<String> messages = Collections.synchronizedList(new ArrayList<String>());

	public static List<String> getMessages() {
		return new ArrayList<String>(messages);
	}

	public static String getFirstMessage() {
		
		if (messages.size() > 0) {
			return messages.remove(0);
		}
		return null;
	}
	
	public static boolean isMessageInQueue() {
		return messages.size() > 0;
	}
	
	public static void addMessage(String message) {
		// TODO add split message basic logic 
		// too wide or too much new lines should be splitted into separate message
		messages.add(message);
	}
}
