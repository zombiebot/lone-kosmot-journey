package com.xeranas.lkj.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.xeranas.lkj.util.Global;
import com.xeranas.lkj.util.MessageQueue;

public class TextActor extends Actor {

	private static final String DEFAULT_TEXT = "<QUIET>";
	
	private BitmapFont font;
	private BitmapFont operatorFont;

	private Camera camera;
	private float offsetX;
	private float offsetY;
	private boolean isLastMsgRead = true;
	private String currentMsg = DEFAULT_TEXT;
	
	public TextActor(Camera camera, float offsetX, float offsetY) {
		this.camera = camera;
		this.offsetX = offsetX * Global.PIXELS_PER_METER;
		this.offsetY = offsetY * Global.PIXELS_PER_METER;
		
		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/FifteenNarrow.ttf"));
		FreeTypeFontParameter parameter = new FreeTypeFontParameter();
		parameter.size = 26;
		parameter.minFilter = TextureFilter.Linear;
		parameter.magFilter = TextureFilter.Linear;
		font = generator.generateFont(parameter);
		
		parameter.size = 36;
		operatorFont = generator.generateFont(parameter);
		generator.dispose();

	}
	
	@Override
	public void draw(Batch batch, float parentAlpha) {
		if (isLastMsgRead && MessageQueue.isMessageInQueue()) {
			currentMsg = MessageQueue.getFirstMessage();
		}
		
		operatorFont.draw(batch, "MAINFRAME:",
				camera.position.x + offsetX, camera.position.y + offsetY);
		
		font.draw(batch, currentMsg,
				camera.position.x + offsetX, camera.position.y + offsetY - 0.6f * Global.PIXELS_PER_METER);
	}

}
